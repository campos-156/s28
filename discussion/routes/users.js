//[SECTION] modules and dependencies
    const exp = require('express');
    const controller = require('../controllers/users');

//[SECTION] Routing Component
    const route = exp.Router();

//[Section] Expose Routing Component
    route.post('/register', (req, res)=>{
        //execute the registerUser() from the controller
        let userInfo = req.body;
        controller.registerUser(userInfo).then(result => {
            res.send(result)
        });
    });

    route.get('/list', (req, res)=>{
        controller.getAllUsers().then(result => {
            res.send(result);
        })
        
    });

    route.put('/:id', (req,res) => {
        let id = req.params.id;
        let userInput = req.body;

        controller.updateUser(id, userInput).then(outcome => {
            res.send(outcome);
        });
    });

    route.delete('/:id', (req,res) => {
        let userId = req.params.id;
        controller.deleteUser(userId).then(outcome => {
            res.send(outcome);
        });
    });

    //Retrieve Single User
    route.get('/:id', (req, res) => {
        let userId = req.params.id;
        controller.getProfile(userId).then(outcome => {
            res.send(outcome);
        });
    });

    



    module.exports = route;