//[SECTION] modules and dependencies
    const express = require('express');
    const controller = require('../controllers/tasks');

//[SECTION] Routing Component
    const route = express.Router();

//[Section] Expose Routing Component
    route.post('/', (req, res)=>{
        //execute the createTask() from the controller
        let taskInfo = req.body;
        controller.createTask(taskInfo).then(result => {
            res.send(result)
        });
    });

    route.get('/', (req, res)=>{
        //Retrive all Tasks
        controller.getAllTask().then(result => {
            res.send(result);
        })
        
    });

    //Retrieve Single Task
    route.get('/:id', (req, res) => {
        let taskId = req.params.id;
        controller.getTask(taskId).then(outcome => {
            res.send(outcome);
        });
    });

    //Update Status
    route.put('/:id', (req,res) => {
        let id = req.params.id;
        let userInput = req.body;

        controller.updateTask(id, userInput).then(outcome => {
            res.send(outcome);
        });
    });

    //Delete Task
    route.delete('/:id', (req,res) => {
        let taskId = req.params.id;
        controller.deleteTask(taskId).then(outcome => {
            res.send(outcome);
        });
    });


    module.exports = route;