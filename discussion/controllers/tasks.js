const Task = require('../models/Task')

//[Section]
    //Create New Task
    module.exports.createTask = (clientInput) => {
        let taskName = clientInput.name
        let newTask = new Task({
            name: taskName
        });
        return newTask.save().then((task, error) => {
            if (error) {
                return 'Saving New Task Failed';
            } else {
                return 'A New Task Successfully Created';
            }
        })
    };
    
    //Retrieve All Tasks
    module.exports.getAllTask = () => {
        return Task.find({}).then(searchResult => {
            return searchResult;
        })
    }

    //Retrieve single Task
    module.exports.getTask = (data) => {
            return Task.findById(data).then(result => {
                return result;
            })
        }

    //Update Task status
    module.exports.updateTask = (taskId, newContent) => {

            let newStatus = newContent.status;

            return Task.findById(taskId).then((foundTask, error) => {
                if (foundTask) {

                    foundTask.status = newStatus;

                    return foundTask.save().then((updatedTask, saveErr) => {
                        if (saveErr) {
                            return false;
                        } else {
                            return updatedTask;
                        }
                    });

                } else {
                    return 'No User Found';
                }
            })
        }

    //Delete Task
    module.exports.deleteTask = (taskId) => {
            return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
                if (removedTask) {
                    return `${removedTask} deleted`;
                } else {
                    return 'No Accounts Removed'; 
                }
            })
        }
