//[SECTION] Dependencies and Modules
    const User = require('../models/User');
    const bcrypt = require('bcrypt');

//[SECTION] User Functionalities
        module.exports.registerUser = (reqBody) => {
            let fName = reqBody.firstName;
            let lName = reqBody.lastName;
            let email = reqBody.email;
            let passW = reqBody.password;

            let newUser = new User ({
                firstName: fName,
                lastName: lName,
                email: email,
                password: bcrypt.hashSync(passW, 10)
            });
            
            return newUser.save().then((user, error) => {
                if (user) {
                    return `A New User ${fName} has been Created`;
                } else {
                    return 'User creation Failed'
                }
            })
        }
        
        module.exports.getProfile = (data) => {
            return User.findById(data).then(result => {
                result.password = '';
                return result;
            })
        }

        module.exports.deleteUser = (userId) => {
            return User.findByIdAndRemove(userId).then((removedUser, err) => {
                if (removedUser) {
                    return removedUser;
                } else {
                    return 'No Accounts Removed'; 
                }
            })
        }

        module.exports.updateUser = (userId, newContent) => {

            let fName = newContent.firstName;
            let lName = newContent.lastName;

            return User.findById(userId).then((foundUser, error) => {
                if (foundUser) {

                    foundUser.firstName = fName;
                    foundUser.lastName = lName;

                    return foundUser.save().then((updatedUser, saveErr) => {
                        if (saveErr) {
                            return false;
                        } else {
                            return updatedUser;
                        }
                    });

                } else {
                    return 'No User Found';
                }
            })
        }

        module.exports.getAllUsers = () => {
        return User.find({}).then(searchResult => {
            return searchResult;
        })
        }